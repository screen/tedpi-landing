<footer class="container-fluid p-0" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">
    <div class="row g-0">
        <div class="the-footer col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start">
                    <?php if ( is_active_sidebar( 'sidebar_footer' ) ) : ?>
                    <div class="footer-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul id="sidebar-footer1" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-2' ) ) : ?>
                    <div class="footer-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul id="sidebar-footer2" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-2' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-3' ) ) : ?>
                    <div class="footer-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul id="sidebar-footer3" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-3' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <?php if ( is_active_sidebar( 'sidebar_footer-4' ) ) : ?>
                    <div class="footer-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <ul id="sidebar-footer4" class="footer-sidebar">
                            <?php dynamic_sidebar( 'sidebar_footer-4' ); ?>
                        </ul>
                    </div>
                    <?php endif; ?>
                    <div class="w-100"></div>
                </div>
            </div>
        </div>
        <div class="footer-copy col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <h5><?php _e('Tedpi by', 'tedpi'); ?> <a href="#"><?php _e('Detector Power', 'tedpi'); ?></a></h5>
            <p><?php echo date('Y'); ?> <?php _e('TEDPI', 'tedpi'); ?> | <?php _e('Best Hobby Metal Detectors, Outdoor Sports, Diving and Drones') ?> | <?php _e('Shop', 'tedpi'); ?></p>
            <p><?php printf(__('All Rights Reserved | Developed by <a href="%s">SMG</a> | <a href="%s">DIGITAL MARKETING AGENCY</a>', 'tedpi'), 'http://www.screenmediagroup.com', 'http://www.screenmediagroup.com'); ?></p>

        </div>
    </div>
</footer>
<?php wp_footer() ?>
</body>

</html>