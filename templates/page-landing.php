<?php
/**
* Template Name: Landing Template
*
* @package tedpi
* @subpackage tedpi-mk01-theme
* @since Mk. 1.0
*/
?>
<?php get_header(); ?>
<?php the_post(); ?>
<main class="container-fluid p-0" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row g-0">
        <section class="main-banner-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
            <div class="container">
                <div class="row">
                    <?php $image_bg = wp_get_attachment_image_src(get_post_meta(get_the_ID(), 'tdp_home_hero_bg_id', true), 'full', false); ?>
                    <div class="main-banner-content col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" style="background: url(<?php echo $image_bg[0]; ?>)">
                        <div class="container">
                            <div class="row align-items-center">
                                <div class="main-banner-text col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-1 order-lg-1 order-md-1 order-sm-2 order-2" data-aos="fade" data-aos-delay="300">
                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tdp_home_hero_title', true)); ?>
                                    <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tdp_home_hero_desc', true)); ?>
                                </div>
                                <div class="main-banner-img col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 order-xl-6 order-lg-6 order-md-6 order-sm-1 order-1" data-aos="fade" data-aos-delay="450">
                                    <?php echo wp_get_attachment_image(get_post_meta(get_the_ID(), 'tdp_home_hero_image_id', true), 'full', array('class' => 'img-fluid'));?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-equip-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-equip-item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12 align-self-start" data-aos="fade-up" data-aos-delay="150">
                        <div class="intro">
                            <h2><?php echo get_post_meta(get_the_ID(), 'tdp_home_equip_title', true); ?></h2>
                            <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tdp_home_equip_desc', true)); ?>
                        </div>
                    </div>
                    <?php $group_equip = get_post_meta(get_the_ID(), 'tdp_home_equip_group', true); ?>
                    <?php if (!empty($group_equip)) : ?>
                    <?php $i = 1; ?>
                    <?php foreach ($group_equip as $item) { ?>
                    <?php $delay = ($i < 2) ? (300 * $i) : (150 * $i); ?>
                    <div class="main-equip-item col-xl-4 col-lg-4 col-md-6 col-sm-12 col-12" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
                        <div class="main-equip-item-wrapper">
                            <?php $image_bg = wp_get_attachment_image_src($item['image_id'], 'full', false); ?>
                            <div class="picture" style="background: url(<?php echo $image_bg[0]; ?>)">
                                <h2><?php echo $item['title']  ?></h2>
                            </div>
                            <div class="desc">
                                <?php echo apply_filters('the_content', $item['desc']); ?>
                            </div>
                            <div class="subtitle">
                                <h3><?php _e('GET FREE ADVICE!', 'tedpi'); ?></h3>
                            </div>
                            <div class="actions">
                                <a href="" data-bs-toggle="modal" data-bs-target="#exampleModal"><img src="<?php echo get_template_directory_uri(); ?>/images/icon-email.png" alt="Email" /></a>
                                <a href=""><img src="<?php echo get_template_directory_uri(); ?>/images/icon-phone.png" alt="Email" /></a>
                            </div>
                            <div class="footer">
                                <p><?php _e('Hablamos Español', 'tedpi'); ?></p>
                            </div>
                        </div>
                    </div>
                    <?php $i++; } ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="main-brands-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center justify-content-center">
                    <div class="main-brands-title col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tdp_home_brands_title', true); ?></h2>
                    </div>
                    <?php $brands = get_post_meta(get_the_ID(), 'tdp_home_brands_logos', true); ?>
                    <?php if (!empty($brands)) : ?>
                    <div class="main-brands-slider-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="swiper swiper-brands">
                            <div class="swiper-wrapper">
                                <?php foreach ($brands as $key => $value) { ?>
                                <div class="swiper-slide">
                                    <?php echo wp_get_attachment_image($key, 'full', false, array('class' => 'img-fluid')); ?>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="swiper-button-prev"></div>
                            <div class="swiper-button-next"></div>
                        </div>
                    </div>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="main-about-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-center">
                    <div class="main-about-img col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <?php echo wp_get_attachment_image(get_post_meta(get_the_ID(), 'tdp_home_about_image_id', true), 'full', array('class' => 'img-fluid'));?>
                    </div>
                    <div class="main-about-text col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tdp_home_about_title', true); ?></h2>
                        <h4><?php echo get_post_meta(get_the_ID(), 'tdp_home_about_subtitle', true); ?></h4>
                        <?php echo apply_filters('the_content', get_post_meta(get_the_ID(), 'tdp_home_about_desc', true)); ?>
                        <h5><?php echo get_post_meta(get_the_ID(), 'tdp_home_about_footer', true); ?></h5>
                    </div>
                </div>
            </div>
        </section>
        <section class="main-benefits-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container">
                <div class="row align-items-start justify-content-center" data-aos="fade" data-aos-delay="150">
                    <?php $benefits = get_post_meta(get_the_ID(), 'tdp_home_benefits_group', true); ?>
                    <?php if (!empty($benefits)) : ?>
                    <?php $i = 1; ?>
                    <?php foreach ($benefits as $item) { ?>
                    <?php $delay = 150 * $i; ?>
                    <div class="main-benefits-item col-xl-3 col-lg-3 col-md-6 col-sm-12 col-12" data-aos="fade" data-aos-delay="<?php echo $delay; ?>">
                        <?php echo wp_get_attachment_image($item['image_id'], 'full', false, array('class' => 'img-fluid')); ?>
                        <h3><?php echo $item['title']; ?></h3>
                        <?php echo apply_filters('the_content', $item['desc']); ?>
                    </div>
                    <?php $i++; } ?>
                    <?php endif; ?>
                </div>
            </div>
        </section>
        <section class="main-contact-container col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
            <div class="container-fluid">
                <div class="row g-0 align-items-center">
                    <div class="main-content-text col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <h2><?php echo get_post_meta(get_the_ID(), 'tdp_home_contact_title', true); ?></h2>
                        <?php $email = get_post_meta(get_the_ID(), 'tdp_home_contact_email', true); ?>
                        <?php if ($email != '') { ?>
                        <div class="contact-item" data-aos="fade" data-aos-delay="200">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_mail.svg" alt="Email" /> <a href=""><?php echo $email; ?></a>
                        </div>
                        <?php } ?>

                        <?php $phone = get_post_meta(get_the_ID(), 'tdp_home_contact_phones', true); ?>
                        <?php if ($phone != '') { ?>
                        <div class="contact-item" data-aos="fade" data-aos-delay="300">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_phone.svg" alt="phone" /> <span><?php echo $phone; ?></span>
                        </div>
                        <?php } ?>

                        <?php $whatsapp = get_post_meta(get_the_ID(), 'tdp_home_contact_whatsapp', true); ?>
                        <?php if ($whatsapp != '') { ?>
                        <div class="contact-item" data-aos="fade" data-aos-delay="400">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_whatsapp.svg" alt="whatsapp" /> <a href=""><?php echo $whatsapp; ?></a>
                        </div>
                        <?php } ?>

                        <?php $address = get_post_meta(get_the_ID(), 'tdp_home_contact_address', true); ?>
                        <?php if ($address != '') { ?>
                        <div class="contact-item" data-aos="fade" data-aos-delay="500">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/icon_location.svg" alt="location" /> <span><?php echo $address; ?></span>
                        </div>
                        <?php } ?>

                    </div>
                    <div class="main-content-embed col-xl-6 col-lg-6 col-md-12 col-sm-12 col-12" data-aos="fade" data-aos-delay="150">
                        <div class="ratio ratio-4x3">
                            <?php echo get_post_meta(get_the_ID(), 'tdp_home_contact_embed_map', true); ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<!-- Modal -->
<div class="modal custom-modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-body">
                <div class="custom-modal-header">
                    <h5 class="modal-title" id="exampleModalLabel"><?php _e('Get Free Advice', 'tedpi'); ?></h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form class="main-form-container">
                    <div class="container">
                        <div class="row">
                            <div class="main-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <input id="formName" name="formName" class="form-control custom-form-control" type="text" placeholder="<?php _e('Name', 'tedpi'); ?> *" />
                                <small class="danger d-none"></small>
                            </div>
                            <div class="main-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <input id="formLastName" name="formLastName" class="form-control custom-form-control" type="text" placeholder="<?php _e('Last Name', 'tedpi'); ?> *" />
                                <small class="danger d-none"></small>
                            </div>
                            <div class="main-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <input id="formEmail" name="formEmail" class="form-control custom-form-control" type="email" placeholder="<?php _e('Email', 'tedpi'); ?> *" />
                                <small class="danger d-none"></small>
                            </div>
                            <div class="main-form-item col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12">
                                <input id="formPhone" name="formPhone" class="form-control custom-form-control" type="tel" placeholder="<?php _e('Phone', 'tedpi'); ?> *" />
                                <small class="danger d-none"></small>
                            </div>
                            <div class="main-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <textarea id="formMessage" name="formMessage" class="form-control custom-form-control" cols="40" rows="3" placeholder="<?php _e('Comments', 'tedpi'); ?>"></textarea>
                                <small class="danger d-none"></small>
                            </div>
                            <div class="main-form-item col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                                <button type="submit" class="btn btn-md btn-submit"><?php _e('Send', 'tedpi'); ?></button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php get_footer(); ?>