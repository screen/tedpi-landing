<?php
/* PREVENT DIRECT ACCESS */
if (!defined('ABSPATH')) {
    die('Invalid request.');
}

/** Display verbose errors */
if (!defined('IMPORT_DEBUG')) {
    define('IMPORT_DEBUG', WP_DEBUG);
}

if (!class_exists('Tedpi_Landing_Metaboxes')) :
    class Tedpi_Landing_Metaboxes extends customCMB2Class
    {
        public function __construct()
        {
            add_action('cmb2_admin_init', array($this, 'tedpi_landing_custom_metabox'));
        }

        public function tedpi_landing_custom_metabox()
        {
            /* 1.- LANDING: HERO SECTION */
            $cmb_home_hero = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_hero_metabox',
                'title'         => esc_html__('Landing: Main Slider Hero', 'tedpi'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_bg',
                'name'      => esc_html__('Background for Hero', 'tedpi'),
                'desc'      => esc_html__('Upload a background for this section', 'tedpi'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload background', 'tedpi'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_image',
                'name'      => esc_html__('Image for Hero', 'tedpi'),
                'desc'      => esc_html__('Upload an image for this section', 'tedpi'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload Image', 'tedpi'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_title',
                'name'      => esc_html__('Hero Title', 'tedpi'),
                'desc'      => esc_html__('Add a descriptive title for this Hero', 'tedpi'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_hero->add_field(array(
                'id'        => parent::PREFIX . 'home_hero_desc',
                'name'      => esc_html__('Hero Description', 'tedpi'),
                'desc'      => esc_html__('Add a description below title for this Hero', 'tedpi'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            /* 2.- LANDING: VIDEO SECTION */
            $cmb_home_equip = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_equip_metabox',
                'title'         => esc_html__('Landing: Equipment Section', 'tedpi'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));
            
            $cmb_home_equip->add_field(array(
                'id'        => parent::PREFIX . 'home_equip_title',
                'name'      => esc_html__('Section Title', 'tedpi'),
                'desc'      => esc_html__('Add a descriptive title for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_equip->add_field(array(
                'id'        => parent::PREFIX . 'home_equip_desc',
                'name'      => esc_html__('Section Description', 'tedpi'),
                'desc'      => esc_html__('Add a description below title for this Section', 'tedpi'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $group_field_id = $cmb_home_equip->add_field(array(
                'id'            => parent::PREFIX . 'home_equip_group',
                'name'          => esc_html__('Equipment Group', 'tedpi'),
                'description'   => __('Group of Equipments inside this section', 'tedpi'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Equipment {#}', 'tedpi'),
                    'add_button'        => __('Add other Equipment', 'tedpi'),
                    'remove_button'     => __('Remove Equipment', 'tedpi'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this Equipment?', 'tedpi')
                )
            ));

            $cmb_home_equip->add_group_field($group_field_id, array(
                'id'        => 'image',
                'name'      => esc_html__('Equipment Image', 'tedpi'),
                'desc'      => esc_html__('Upload an image for this Equipment', 'tedpi'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload image', 'tedpi'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_equip->add_group_field($group_field_id, array(
                'id'        => 'title',
                'name'      => esc_html__('Equipment Title', 'tedpi'),
                'desc'      => esc_html__('Insert a title for this Equipment', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_equip->add_group_field($group_field_id, array(
                'id'        => 'desc',
                'name'      => esc_html__('Equipment Description', 'tedpi'),
                'desc'      => esc_html__('Insert a description for this Equipment', 'tedpi'),
                'type'      => 'text'
            ));

            /* 3.- LANDING: BRANDS SECTION */
            $cmb_home_brands = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_brands_metabox',
                'title'         => esc_html__('Landing: Benefits Section', 'tedpi'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_brands->add_field(array(
                'id'        => parent::PREFIX . 'home_brands_title',
                'name'      => esc_html__('Section Title', 'tedpi'),
                'desc'      => esc_html__('Add a title for this section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_brands->add_field(array(
                'id'            => parent::PREFIX . 'home_brands_logos',
                'name'          => esc_html__('Brand Logos', 'tedpi'),
                'desc'          => esc_html__('Upload logos for this section', 'tedpi'),
                'type'          => 'file_list',
                'preview_size'  => array(100, 100),
                'query_args'    => array('type' => 'image'),
                'text'          => array(
                    'add_upload_files_text' => 'Upload',
                    'remove_image_text'     => 'Remove',
                    'file_text'             => 'Image',
                    'file_download_text'    => 'Download',
                    'remove_text'           => 'Remove'
                )
            ));
           

            /* 4.- LANDING: ABOUT SECTION */
            $cmb_home_about = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_about_metabox',
                'title'         => esc_html__('Landing: About Section', 'tedpi'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_image',
                'name'      => esc_html__('Image for Hero', 'tedpi'),
                'desc'      => esc_html__('Upload an image for this Section', 'tedpi'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload Image', 'tedpi'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_title',
                'name'      => esc_html__('Section Title', 'tedpi'),
                'desc'      => esc_html__('Add a title for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_subtitle',
                'name'      => esc_html__('Section Subtitle', 'tedpi'),
                'desc'      => esc_html__('Add a subtitle for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_desc',
                'name'      => esc_html__('Section Description', 'tedpi'),
                'desc'      => esc_html__('Add a description below title for this Section', 'tedpi'),
                'type'      => 'wysiwyg',
                'options'   => array(
                    'textarea_rows' => get_option('default_post_edit_rows', 2),
                    'teeny'         => false
                )
            ));

            $cmb_home_about->add_field(array(
                'id'        => parent::PREFIX . 'home_about_footer',
                'name'      => esc_html__('Section Footer title', 'tedpi'),
                'desc'      => esc_html__('Add a footer title for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            /* 5.- LANDING: BENEFITS SECTION */
            $cmb_home_benefits = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_benefits_metabox',
                'title'         => esc_html__('Landing: Benefits Section', 'tedpi'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));
            
            $group_field_id = $cmb_home_benefits->add_field(array(
                'id'            => parent::PREFIX . 'home_benefits_group',
                'name'          => esc_html__('Benefits Group', 'tedpi'),
                'description'   => __('Group of Benefits inside this section', 'tedpi'),
                'type'          => 'group',
                'options'       => array(
                    'group_title'       => __('Benefit {#}', 'tedpi'),
                    'add_button'        => __('Add other Benefit', 'tedpi'),
                    'remove_button'     => __('Remove Benefit', 'tedpi'),
                    'sortable'          => true,
                    'closed'            => true,
                    'remove_confirm'    => esc_html__('Are you sure to remove this Benefit?', 'tedpi')
                )
            ));

            $cmb_home_benefits->add_group_field($group_field_id, array(
                'id'        => 'image',
                'name'      => esc_html__('Benefit Image', 'tedpi'),
                'desc'      => esc_html__('Upload an image for this Benefit', 'tedpi'),
                'type'      => 'file',

                'options' => array(
                    'url' => false
                ),
                'text'    => array(
                    'add_upload_file_text' => esc_html__('Upload image', 'tedpi'),
                ),
                'query_args' => array(
                    'type' => array(
                        'image/gif',
                        'image/jpeg',
                        'image/png'
                    )
                ),
                'preview_size' => 'thumbnail'
            ));

            $cmb_home_benefits->add_group_field($group_field_id, array(
                'id'        => 'title',
                'name'      => esc_html__('Benefit Title', 'tedpi'),
                'desc'      => esc_html__('Insert a title for this Benefit', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_benefits->add_group_field($group_field_id, array(
                'id'        => 'desc',
                'name'      => esc_html__('Benefit Description', 'tedpi'),
                'desc'      => esc_html__('Insert a description for this Benefit', 'tedpi'),
                'type'      => 'text'
            ));

            /* 6.- LANDING: CONTACT SECTION */
            $cmb_home_contact = new_cmb2_box(array(
                'id'            => parent::PREFIX . 'home_contact_metabox',
                'title'         => esc_html__('Landing: Contact Section', 'tedpi'),
                'object_types'  => array('page'),
                'show_on'       => array('key' => 'page-template', 'value' => 'templates/page-landing.php'),
                'context'       => 'normal',
                'priority'      => 'high',
                'show_names'    => true,
                'cmb_styles'    => true,
                'closed'        => false
            ));

            $cmb_home_contact->add_field(array(
                'id'        => parent::PREFIX . 'home_contact_embed_map',
                'name'      => esc_html__('Section Embed Map', 'tedpi'),
                'desc'      => esc_html__('Add a Embed Map for this Section', 'tedpi'),
                'type'      => 'textarea_code'
            ));

            $cmb_home_contact->add_field(array(
                'id'        => parent::PREFIX . 'home_contact_title',
                'name'      => esc_html__('Section Title', 'tedpi'),
                'desc'      => esc_html__('Add a title for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_contact->add_field(array(
                'id'        => parent::PREFIX . 'home_contact_email',
                'name'      => esc_html__('Email Contact', 'tedpi'),
                'desc'      => esc_html__('Add a Email Contact for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_contact->add_field(array(
                'id'        => parent::PREFIX . 'home_contact_phones',
                'name'      => esc_html__('Phones Contact', 'tedpi'),
                'desc'      => esc_html__('Add a Phone/Phones Contact for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_contact->add_field(array(
                'id'        => parent::PREFIX . 'home_contact_whatsapp',
                'name'      => esc_html__('WhastApp Contact', 'tedpi'),
                'desc'      => esc_html__('Add a WhastApp Contact for this Section', 'tedpi'),
                'type'      => 'text'
            ));

            $cmb_home_contact->add_field(array(
                'id'        => parent::PREFIX . 'home_contact_address',
                'name'      => esc_html__('Address Contact', 'tedpi'),
                'desc'      => esc_html__('Add a Address Contact for this Section', 'tedpi'),
                'type'      => 'text'
            ));
        }
    }
endif;

new Tedpi_Landing_Metaboxes;