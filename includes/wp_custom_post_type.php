<?php

if (!defined('ABSPATH')) {
	die('Invalid request.');
}

/*
function tedpi_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Clientes', 'Post Type General Name', 'tedpi' ),
		'singular_name'         => _x( 'Cliente', 'Post Type Singular Name', 'tedpi' ),
		'menu_name'             => __( 'Clientes', 'tedpi' ),
		'name_admin_bar'        => __( 'Clientes', 'tedpi' ),
		'archives'              => __( 'Archivo de Clientes', 'tedpi' ),
		'attributes'            => __( 'Atributos de Cliente', 'tedpi' ),
		'parent_item_colon'     => __( 'Cliente Padre:', 'tedpi' ),
		'all_items'             => __( 'Todos los Clientes', 'tedpi' ),
		'add_new_item'          => __( 'Agregar Nuevo Cliente', 'tedpi' ),
		'add_new'               => __( 'Agregar Nuevo', 'tedpi' ),
		'new_item'              => __( 'Nuevo Cliente', 'tedpi' ),
		'edit_item'             => __( 'Editar Cliente', 'tedpi' ),
		'update_item'           => __( 'Actualizar Cliente', 'tedpi' ),
		'view_item'             => __( 'Ver Cliente', 'tedpi' ),
		'view_items'            => __( 'Ver Clientes', 'tedpi' ),
		'search_items'          => __( 'Buscar Cliente', 'tedpi' ),
		'not_found'             => __( 'No hay resultados', 'tedpi' ),
		'not_found_in_trash'    => __( 'No hay resultados en Papelera', 'tedpi' ),
		'featured_image'        => __( 'Imagen del Cliente', 'tedpi' ),
		'set_featured_image'    => __( 'Colocar Imagen del Cliente', 'tedpi' ),
		'remove_featured_image' => __( 'Remover Imagen del Cliente', 'tedpi' ),
		'use_featured_image'    => __( 'Usar como Imagen del Cliente', 'tedpi' ),
		'insert_into_item'      => __( 'Insertar en Cliente', 'tedpi' ),
		'uploaded_to_this_item' => __( 'Cargado a este Cliente', 'tedpi' ),
		'items_list'            => __( 'Listado de Clientes', 'tedpi' ),
		'items_list_navigation' => __( 'Navegación del Listado de Cliente', 'tedpi' ),
		'filter_items_list'     => __( 'Filtro del Listado de Clientes', 'tedpi' ),
	);
	$args = array(
		'label'                 => __( 'Cliente', 'tedpi' ),
		'description'           => __( 'Portafolio de Clientes', 'tedpi' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies'            => array( 'tipos-de-clientes' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 15,
		'menu_icon'             => 'dashicons-businessperson',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
		'show_in_rest'          => true,
	);
	register_post_type( 'clientes', $args );

}

add_action( 'init', 'tedpi_custom_post_type', 0 );
*/
