let swiperBrands = document.getElementsByClassName('swiper-brands');
/* CUSTOM ON LOAD FUNCTIONS */
function tedpiCustomLoad() {
    "use strict";
    console.log('Functions Correctly Loaded');

    if (swiperBrands != null) {
        const swiperBrandsWrapper = new Swiper('.swiper-brands', {
            direction: 'horizontal',
            loop: true,
            autoplay: {
                delay: 5000,
            },
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
            breakpoints: {
                0: {
                    spaceBetween: 5,
                    slidesPerView: 1
                },
                600: {
                    spaceBetween: 5,
                    slidesPerView: 3
                },
                768: {
                    spaceBetween: 15,
                    slidesPerView: 4
                },
                1024: {
                    spaceBetween: 20,
                    slidesPerView: 5
                },
                1200: {
                    spaceBetween: 20,
                    slidesPerView: 7
                }
            }
        });
    }
}

document.addEventListener("DOMContentLoaded", tedpiCustomLoad, false);

AOS.init();